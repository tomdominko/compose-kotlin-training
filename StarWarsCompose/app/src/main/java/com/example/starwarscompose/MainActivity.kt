package com.example.starwarscompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.Dp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.starwarscompose.ui.home.Home
import com.example.starwarscompose.ui.select.Select
import com.example.starwarscompose.ui.theme.StarWarsComposeTheme
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.*
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


val baseUrl = "https://swapi.dev/api/"

val personList: MutableList<SWPerson> = mutableListOf()


private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

val yeet = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(baseUrl)
    .build()

var i = 1;

interface SWApiService {

    @GET("people/")

    fun getProperties(@Query("page") pageNum : String):
            Call<SWResults>

}

object SWApi {
    val retrofitService : SWApiService =
        yeet.create(SWApiService::class.java)
}

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        yeet(i.toString())

        setContent {
            val navController = rememberNavController()
            StarWarsComposeTheme() {
                NavHost(navController = navController, startDestination = "Home") {
                    composable("Home") { Home(navController) }
                    composable("Select") { Select(navController, personList) }
                }
            }
        }
    }

    fun yeet(i : String) {
        SWApi.retrofitService.getProperties(i).enqueue(object : Callback<SWResults> {
            override fun onFailure(call: Call<SWResults>, t: Throwable) {
                println("Failure: " + t.message)
            }

            override fun onResponse(call: Call<SWResults>, response: Response<SWResults>) {
                println(response.body()!!.next)

                    for (person: SWPerson in response.body()!!.results) {
                        personList.add(person)
                    }
                if(response.body()!!.next != null){

                    var nextPage = i.toInt()
                    nextPage++

                    yeet(nextPage.toString())
                }
                else{
                    println("BREAK")
                    for(person : SWPerson in personList){
                        println(person)
                    }

                }
            }
        })
    }
}



@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    StarWarsComposeTheme {

    }
}

data class SWResults(
    @Json(name = "next")
//  accounts for no more pages left (null)
    val next: String?,

    @Json(name = "results")
    val results: List<SWPerson>,
)

data class SWPerson(
    @Json(name = "name")
    val name: String,

    @Json(name = "height")
    val height: String,

    @Json(name = "mass")
    val mass: String,

    @Json(name = "hair_color")
    val hair_color: String
    )

