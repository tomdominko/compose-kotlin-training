package com.example.starwarscompose.ui.select

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.navigation.NavController
import com.example.starwarscompose.SWPerson
import com.example.starwarscompose.ui.select.person.PersonList

@Composable
fun Select(navController: NavController, people: List<SWPerson>) {
    Surface(Modifier.fillMaxSize(), color = MaterialTheme.colors.primary) {
        Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
            Button(onClick = { navController.navigate("Home") },
                border = BorderStroke(Dp(2F), MaterialTheme.colors.secondary)
            ){
                Text(text = "Go Back to Home", style = TextStyle(color = MaterialTheme.colors.secondary))
            }
            PersonList(people)
        }
    }
}
