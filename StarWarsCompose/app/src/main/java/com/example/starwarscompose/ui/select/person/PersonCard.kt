package com.example.starwarscompose.ui.select.person

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import com.example.starwarscompose.SWPerson
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.starwarscompose.ui.theme.StarWarsComposeTheme


@Composable
fun PersonCard(person : SWPerson){
    Card(backgroundColor = MaterialTheme.colors.onSecondary, border = BorderStroke(Dp(1F), MaterialTheme.colors.secondary)) {

        Column(Modifier.padding(12.dp)){
            Text(person.name, style = TextStyle(fontWeight = FontWeight(1000)))
            Text("Height: " + person.height+"cm")
            Text("Weight: " + person.mass+"kg")
            Text("Hair Color: " + person.hair_color)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    StarWarsComposeTheme {
        PersonCard(person = SWPerson("Test", "175", "250", "Blonde"))
    }
}
