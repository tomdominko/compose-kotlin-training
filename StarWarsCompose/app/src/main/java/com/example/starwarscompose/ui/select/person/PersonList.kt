package com.example.starwarscompose.ui.select.person

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.starwarscompose.SWPerson

@Composable
fun PersonList(people : List<SWPerson>){
    LazyColumn(){
//        people.forEach { person ->
//            PersonCard(person)
//        }
        items(people) {
            PersonCard(it)
        }
    }
}