package com.example.starwarscompose.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Black900 = Color(0xFF000000)
val Black800 = Color(0xFF414141)
val StarWarsYellow = Color(0xFFFFED2B)