package com.example.starwarscompose.ui.home

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.navigation.NavController
import com.example.starwarscompose.R

@Composable
fun Home(navController: NavController) {
    Surface(Modifier.fillMaxSize(), color = MaterialTheme.colors.primary) {
        Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
            Image(
                painter = painterResource(id = R.drawable.starwars),
                contentDescription = "Star Wars Logo" // decorative element
            )
            Spacer(Modifier.size(Dp(100F)))
            Button(onClick = { navController.navigate("Select") },
                border = BorderStroke(Dp(2F), MaterialTheme.colors.secondary)
            ){
                Text(text = "Continue", style = TextStyle(color = MaterialTheme.colors.secondary))
            }
        }
    }
}
